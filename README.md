# Laravel Nova Fresh Tap

A custom Laravel Nova Field for [Fresh Tap](https://bitbucket.org/freshinteractive/fresh-tap/src/main/).

Fresh Tap is a framework-agnostic [Custom Element](https://developer.mozilla.org/en-US/docs/Web/API/Web_components/Using_custom_elements) WYSIWYG Editor build on top of [Tiptap](https://tiptap.dev/) and [Vue](https://vuejs.org/).

## Installation

```
composer require freshinteractive/fresh-tap
```

## Usage

```php
<?php

namespace App\Nova;

...
use Freshinteractive\FreshTap\FreshTap;
...

class YourResource extends Resource
{
    ...
    
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        ...,
        FreshTap::make('Content')
                ->previewUrl('https://your-preview-url-here.com/page-content-will-live-on')
                ->previewSelector('.css-selector')
                ->previewWrapperElement([
                    'element' => 'div',
                    'attributes' => [
                        'class' => 'your-wrapper-class'
                    ]
                ])
                ->keepEmptyParagraphLineBreaks(),
                ->editorStyles("
                    .FreshTapEditor .ProseMirror { display: grid; grid-template-columns: 100%; gap: 20px; }
                ")
    }
}
```

### Notes:

None of the methods are required. Just ```FreshTap::make('Content')``` works.

When using ```->editorStyles()``` make sure that your styles are prepended with ```.FreshTapEditor .ProseMirror```. These styles will only be applied to the instance of the editor that it is implemented on.
