Nova.booting((Vue, router, store) => {
  Vue.component('index-fresh-tap', require('./components/IndexField'))
  Vue.component('detail-fresh-tap', require('./components/DetailField'))
  Vue.component('form-fresh-tap', require('./components/FormField'))
})
