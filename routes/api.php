<?php

use Freshinteractive\FreshTap\Http\Controllers\ImageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/image/upload', [ImageController::class, 'upload']);
