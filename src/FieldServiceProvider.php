<?php

namespace Freshinteractive\FreshTap;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class FieldServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->booted(function () {
            $this->routes();
        });

        Nova::booted(function () {
            Nova::theme(asset('/freshinteractive/fresh-tap/overrides.css'));
        });

        $this->publishes([
            __DIR__.'/../resources/css' => public_path('freshinteractive/fresh-tap'),
        ], 'public');

        Nova::serving(function (ServingNova $event) {
            Nova::script('fresh-tap', __DIR__.'/../dist/js/field.js');
            Nova::style('fresh-tap', __DIR__.'/../dist/css/field.css');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register the field's routes.
     *
     * @return void
     */
    protected function routes()
    {
        if ($this->app->routesAreCached()) {
            return;
        }

        Route::prefix('nova-vendor/fresh-tap')
            ->group(__DIR__.'/../routes/api.php');
    }
}
