<?php

namespace Freshinteractive\FreshTap;

use Laravel\Nova\Fields\Field;

class FreshTap extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'fresh-tap';

    public $showOnIndex = false;

    /**
     * @param string $css
     * @return FreshTap
     */
    public function editorStyles(string $css): FreshTap
    {
        return $this->withMeta(['editorStyles' => $css]);
    }

    /**
     * @param string $url
     * @return FreshTap
     */
    public function previewUrl(string $url): FreshTap
    {
        return $this->withMeta(['previewUrl' => $url]);
    }

    /**
     * @param string $selector
     * @return FreshTap
     */
    public function previewSelector(string $selector): FreshTap
    {
        return $this->withMeta(['previewSelector' => $selector ]);
    }

    /**
     * @param array $wrapperElement
     * @return FreshTap
     */
    public function previewWrapperElement(array $wrapperElement): FreshTap
    {
        return $this->withMeta(['previewWrapperElement' => $wrapperElement]);
    }

    /**
     * @param bool $keepEmptyParagraphLineBreaks
     * @return FreshTap
     */
    public function keepEmptyParagraphLineBreaks(bool $keepEmptyParagraphLineBreaks = true): FreshTap
    {
        return $this->withMeta(['keepEmptyParagraphLineBreaks' => $keepEmptyParagraphLineBreaks]);
    }
}
