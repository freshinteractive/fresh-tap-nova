<?php

namespace Freshinteractive\FreshTap\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function upload(Request $request)
    {
        $request->validate([
            'file' => 'file|required'
        ]);

        $path = $request->file('file')->store('fresh-tap');

        return Storage::url($path);
    }
}
